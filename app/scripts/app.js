/*global angular, console, window, setTimeout, $*/
(function () {
    "use strict";
    var app = angular.module('App', []);

    app.controller('ctrl', ['$scope', function ($scope) {
        $scope.boilerplate = "boilerplate";
    }]);
}());
